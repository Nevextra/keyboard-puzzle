﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class PuzzleLogic : MonoBehaviour {
	
	public List<string> password = new List<string> ();
	public List<GameObject> keys = new List<GameObject> ();
	GameObject scribe;
	public string pushed;
	public int progress = 0;
	public bool complete = false;
	public string passwordstr;
	public string str;

	// Use this for initialization
	void Start () {
		logicGates (passwordstr);

		scribe = GameObject.FindWithTag("KeyboardDisplay");

		keys.Add(GameObject.Find("qkey"));keys.Add(GameObject.Find("wkey"));keys.Add(GameObject.Find("ekey"));
		keys.Add(GameObject.Find("rkey"));keys.Add(GameObject.Find("tkey"));keys.Add(GameObject.Find("ykey"));
		keys.Add(GameObject.Find("ukey"));keys.Add(GameObject.Find("ikey"));keys.Add(GameObject.Find("okey"));
		keys.Add(GameObject.Find("pkey"));keys.Add(GameObject.Find("akey"));keys.Add(GameObject.Find("skey"));
		keys.Add(GameObject.Find("dkey"));keys.Add(GameObject.Find("fkey"));keys.Add(GameObject.Find("gkey"));
		keys.Add(GameObject.Find("hkey"));keys.Add(GameObject.Find("jkey"));keys.Add(GameObject.Find("kkey"));
		keys.Add(GameObject.Find("lkey"));keys.Add(GameObject.Find("zkey"));keys.Add(GameObject.Find("xkey"));
		keys.Add(GameObject.Find("ckey"));keys.Add(GameObject.Find("vkey"));keys.Add(GameObject.Find("bkey"));
		keys.Add(GameObject.Find("nkey"));keys.Add(GameObject.Find("mkey"));

		scribe.GetComponent<DisplayInput> ().DrawLetter (password.Count);

	}
	
	// Update is called once per frame
	void Update () {
		pushed = Input.inputString;

		if (progress == password.Count) {
			complete = true;
			Complete ();
		} else {
			logicGate ();
		}
	}

	void logicGates(string strComplete){
		for (int i = 0; i < passwordstr.Length; i++) {
			while (i < strComplete.Length) {
				str += strComplete [i++];
				Debug.Log (i);
				password.Add (str.Remove(0,i-1));
			}
		}
	}


	void logicGate(){
		
		foreach (GameObject key in keys) {
			if (pushed + "key" != key.name) {
				if (key.gameObject.GetComponent<CorrectHit> ().correct == false) {
					key.gameObject.GetComponent<Image> ().color = Color.gray;
				}
			}
		}

		if (pushed == password [progress]) {
			scribe.GetComponent<DisplayInput> ().replace (progress, pushed);
			GameObject.Find (pushed + "key").GetComponent<CorrectHit> ().correct = true;
			progress++;
		}

		if (pushed != "") {
			if (GameObject.Find (pushed + "key").GetComponent<CorrectHit> ().correct == true) {
				GameObject.Find (pushed + "key").GetComponent<Image> ().color = Color.green;
				}else if(pushed != password [progress]) {
				GameObject.Find (pushed + "key").GetComponent<Image> ().color = Color.red;
			}
		}
	}

	void Complete(){
		Debug.Log ("finished");
		foreach (GameObject key in keys) {
			for (int i = 0; i < password.Count; i++) {
				if (password [i] + "key" == key.gameObject.name) {
					GameObject.Find (password [i] + "key").GetComponent<Image> ().color = Color.green;
				}
			}
		}
	}
}
