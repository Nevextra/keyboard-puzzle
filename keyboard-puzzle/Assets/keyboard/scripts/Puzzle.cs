﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Puzzle : MonoBehaviour {
	public List<string> password = new List<string> ();
	public List<GameObject> keys = new List<GameObject> ();
	public GameObject scribe;
	public string pushed;
	public int progress = 0;
	public bool complete = false;
	public string passwordstr;
	public string str;
    public Color custom = new Color(0.000f, 0.549f, 1.000f, 1.000f);

    // Use this for initialization
    void Start () {
		logicGates (passwordstr);

		scribe = GameObject.FindWithTag("KeyboardDisplay");

		keys.Add(GameObject.Find("qkey"));keys.Add(GameObject.Find("wkey"));keys.Add(GameObject.Find("ekey"));
		keys.Add(GameObject.Find("rkey"));keys.Add(GameObject.Find("tkey"));keys.Add(GameObject.Find("ykey"));
		keys.Add(GameObject.Find("ukey"));keys.Add(GameObject.Find("ikey"));keys.Add(GameObject.Find("okey"));
		keys.Add(GameObject.Find("pkey"));keys.Add(GameObject.Find("akey"));keys.Add(GameObject.Find("skey"));
		keys.Add(GameObject.Find("dkey"));keys.Add(GameObject.Find("fkey"));keys.Add(GameObject.Find("gkey"));
		keys.Add(GameObject.Find("hkey"));keys.Add(GameObject.Find("jkey"));keys.Add(GameObject.Find("kkey"));
		keys.Add(GameObject.Find("lkey"));keys.Add(GameObject.Find("zkey"));keys.Add(GameObject.Find("xkey"));
		keys.Add(GameObject.Find("ckey"));keys.Add(GameObject.Find("vkey"));keys.Add(GameObject.Find("bkey"));
		keys.Add(GameObject.Find("nkey"));keys.Add(GameObject.Find("mkey"));
	}
	
	// Update is called once per frame
	void Update () {
		getKey ();
		if (progress == password.Count) {
			Complete ();
		} else {
			logicGate (pushed);
		}
	}

	public void getclick(string PassIn){
		logicGate (PassIn);
	}

	void getKey(){
		if (Input.GetKey (KeyCode.LeftShift)) {
			pushed = Input.inputString.ToUpper();
		} else {
			pushed = Input.inputString;
		}
	}

	void logicGates(string strComplete){
		for (int i = 0; i < passwordstr.Length; i++) {
			while (i < strComplete.Length) {
				str += strComplete [i++];
				password.Add (str.Remove(0,i-1));
			}
		}
	}


	void logicGate(string pushing){
		if (pushing == password [progress]) {
			scribe.GetComponent<Display>().WriteText(pushing);
			GameObject.Find (pushing.ToLower() + "key").GetComponent<CorrectHit> ().correct = true;
			progress++;
		}

		if (pushing != "") {
            if (Input.GetKey(KeyCode.LeftShift)) {
                pushing = pushing.ToLower();
            }
            if (GameObject.Find(pushing+ "key").GetComponent<CorrectHit>().correct == true) {
                GameObject.Find(pushing+ "key").GetComponentInChildren<Image>().color = Color.green;
                Debug.Log("green");
            } else if (pushing != password[progress]) {
                Debug.Log("wrong");
                GameObject.Find(pushing + "key").GetComponentInChildren<Image>().color = Color.red;
                StartCoroutine(bounce(pushing));
            }    
        }
	}

	void Complete(){
		complete = true;
		Debug.Log ("finished");
		foreach (GameObject key in keys) {
			for (int i = 0; i < password.Count; i++) {
				if (password [i].ToLower() + "key" == key.gameObject.name) {
					GameObject.Find (password [i].ToLower() + "key").GetComponentInChildren<Image> ().color = Color.green;
				}
			}
		}
	}

    IEnumerator bounce(string pushing) {
        yield return new WaitForSeconds(0.3f);
        GameObject.Find(pushing + "key").GetComponentInChildren<Image>().color = custom;
    }
}
