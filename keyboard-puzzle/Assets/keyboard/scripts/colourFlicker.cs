﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class colourFlicker : MonoBehaviour {
	public Color flicker1;
	public Color flicker2;
	bool flip;
	// Use this for initialization
	void Start () {
		flip = true;
		flicker1 = Color.blue;
		flicker2 = Color.cyan;
		InvokeRepeating ("flipcolor", 1.0f, 1.0f);
	}
	
	// Update is called once per frame
	void Update () {
		if (flip == true) {
			GetComponent<Image> ().color = flicker1;
		}
		if (flip == false) {
			GetComponent<Image> ().color = flicker2;
		}


	}

	void flipcolor(){
		flip = !flip;
	}
}
