﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ClickListener : MonoBehaviour {
	GameObject master;
	public Button ThisClick;
	// Use this for initialization
	void Start () {
		master = GameObject.FindWithTag ("KeyboardMaster");
		ThisClick = gameObject.GetComponent<Button> ();
		ThisClick.onClick.AddListener (Task);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Task(){
		//Debug.Log (gameObject.name.Substring(0,1));
		master.GetComponent<Puzzle> ().getclick (gameObject.name.Substring (0, 1));
	}
}
